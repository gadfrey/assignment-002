<?php

namespace AppBundle\Entity;

use AppBundle\Lib\StaticData;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Ramsey\Uuid\Exception\UnsatisfiedDependencyException;
use Ramsey\Uuid\Uuid;


/**
 * @ORM\Entity(repositoryClass="AppBundle\Repository\CsvFileRepository")
 * @ORM\Table(name="csv_file")
 */
class CsvFile
{
    /**
     * @var int
     *
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=255)
     */
    protected $uuid;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=255)
     */
    protected $name;

    /**
     * @var \DateTime
     *
     * @ORM\Column(type="datetime")
     */
    protected $uploadDate;

    /**
     * @var ArrayCollection
     *
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\CsvCell", mappedBy="csvFile", cascade={"persist"})
     */
    protected $cells;

    protected $headerRow;

    protected $cellRows;

    protected $size;

    public function __construct()
    {
        try {
            $this->uuid = Uuid::uuid4()->toString();
        } catch (UnsatisfiedDependencyException $exception) {
            throw new \RuntimeException($exception->getMessage());
        }
        $this->uploadDate = new \DateTime('now');
        $this->cells      = new ArrayCollection();
        $this->size       = 0;
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getUuid()
    {
        return $this->uuid;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     * @return CsvFile
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getUploadDate()
    {
        return $this->uploadDate;
    }

    /**
     * @param \DateTime $uploadDate
     * @return CsvFile
     */
    public function setUploadDate($uploadDate)
    {
        $this->uploadDate = $uploadDate;

        return $this;
    }

    /**
     * @return ArrayCollection
     */
    public function getCells()
    {
        return $this->cells;
    }

    /**
     * @param CsvCell $cell
     */
    public function addCell(CsvCell $cell)
    {
        $this->size++;
        $this->cells->add($cell);//add to the array collection;
        $cell->setCsvFile($this);
    }

    /**
     *
     */
    public function setHeaderRow()
    {
        $headers = $this->getEachRow(1);
        $this->headerRow = $headers;
    }

    /**
     * @return mixed
     */
    public function getHeaderRow()
    {
        return $this->headerRow;
    }

    /**
     * @return mixed
     */
    public function getCellRows()
    {
        return $this->cellRows;
    }

    /**
     *
     */
    public function setCellRows()
    {
        $rows = [];
        $numOfColumns = sizeof($this->getEachRow(1));
        $size = ($this->cells->count() / $numOfColumns)+1;
        for($i=2; $i<$size; $i++){
            $hold = $this->getEachRow($i);
            if(!$hold->isEmpty()) {
                $rows[$i] = $hold;
            }
        }
        $this->cellRows = $rows;
    }

    public function getSize(){
        return $this->size;
    }

    /**
     * @param $index
     * @return ArrayCollection
     */
    public function getEachRow($index){
        $this->index = $index;
        return $this->cells->filter(function($cell){
            return $cell->getRow() === $this->index;
        });
    }

    /**
     * @return int
     */
    public function numberOfRows(){
        $count = 0;
        foreach($this->cells as $key => $value){
            $count++;
        }
       return $count;
    }
}